package soft.androidhelicoptergame;

import android.view.SurfaceHolder;
import android.graphics.Canvas;

/**
 * Created by Sam on 30-Apr-16.
 */
public class MainThread extends Thread {
    public static Canvas canvas;

    private int framesPerSecond = 30;
    private double averageFramesPerSecond;
    private boolean running;

    private SurfaceHolder surfaceHolder;
    private Helicopter_GamePanel gamePanel;

    public MainThread(SurfaceHolder surfaceHolder, Helicopter_GamePanel gamePanel)
    {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
    }
    @Override
    public void run()
    {
        long startTime;
        long timeMillis;
        long waitTime;
        long totalTime = 0;
        int frameCount = 0;
        long targetTime = 1000 / framesPerSecond;

        while (running)
        {
            startTime = System.nanoTime();
            canvas = null;

            try
            {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder)
                {
                    this.gamePanel.update();
                    this.gamePanel.draw(canvas);
                }
            }

            catch (Exception ex)
            {

            }

            finally
            {
                if (canvas != null)
                {
                    try
                    {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }

                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                }

                timeMillis = (System.nanoTime() - startTime) / 1000000;
                waitTime = targetTime - timeMillis;

                try
                {
                    this.sleep(waitTime);
                }

                catch (Exception ex)
                {

                }

                totalTime += System.nanoTime() - startTime;
                frameCount++;
                if (frameCount == framesPerSecond)
                {

                    averageFramesPerSecond = 1000 / ((totalTime / frameCount) / 1000000);
                    frameCount = 0;
                    totalTime = 0;

                    //System.out.println(averageFramesPerSecond);
                    //System.out.println(Player.score);
                }
            }
        }

    }


    public void setRunning(boolean b) {
    running = b;


    }
}
