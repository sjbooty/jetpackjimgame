package soft.androidhelicoptergame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Sam on 03/05/2016.
 */
public class Main_Menu extends Activity implements View.OnClickListener {

    Button playButton = null;
    Button highscoresButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        playButton = (Button) findViewById(R.id.playGameButton);
        highscoresButton = (Button) findViewById(R.id.highscoresButton);


        playButton.setOnClickListener(this);
        highscoresButton.setOnClickListener(this);


    }


    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent(this, Helicopter_Main.class);
        startActivity(intent);
    }
}
