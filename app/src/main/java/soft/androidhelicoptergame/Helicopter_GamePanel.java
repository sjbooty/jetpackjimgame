package soft.androidhelicoptergame;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.test.suitebuilder.annotation.Smoke;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Random;

import static soft.androidhelicoptergame.R.*;


/**
 * Created by Sam on 30-Apr-16.
 */
public class Helicopter_GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    public static final int WIDTH = 856;
    public static final int HEIGHT = 480;
    public static final int MOVESPEED = -20; //the speed the background will move at(to simulate the players movement)

    private long smokeStartTime;            //the time when the last smoke was made
    private long obstacleStartTime;         //the time when the last obstacle was made


    private MainThread thread;
    private Background bg;
    private Player player;

    private ArrayList<HelicopterSmoke> smoke;
    private ArrayList<Obstacle> obstacles;
    private ArrayList<UpperBorder> upperBorder;
    private ArrayList<BottomBorder> bottomBorder;

    private Random rand = new Random();
    private int maxBorderHeight;
    private int minBorderHeight;
    private int difficultyProgression = 20;  //Difficulty modifier
    private boolean topDown = true;
    private boolean bottomDown = true;

    public Helicopter_GamePanel(Context context)
    {
        super(context);
        getHolder().addCallback(this);                  //adds the callback for surfaceholder

        thread = new MainThread(getHolder(), this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        boolean retry = true;
        int counter = 0;                //Stops an infinite while loop
        while (retry && counter<1000)
        {
            counter++;
            try                         //stop the thread running if the player leaves the page
            {
                thread.setRunning(false);
                thread.join();
                retry = false;
            }

            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }

        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        bg = new Background(BitmapFactory.decodeResource(getResources(), drawable.gamebg1));                //Takes in the background image
        player = new Player(BitmapFactory.decodeResource(getResources(), drawable.helicopter), 65, 25, 3);  //Takes in the player avatar, the images frame size and the number of frames to display for an animation
        smoke = new ArrayList<HelicopterSmoke>();
        obstacles = new ArrayList<Obstacle>();
        upperBorder = new ArrayList<UpperBorder>();
        bottomBorder = new ArrayList<BottomBorder>();


        smokeStartTime = System.nanoTime();   //Start time for both the obstacles, the smoke and the borders
        obstacleStartTime = System.nanoTime();

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN)            //if the player is playing and not pressing the touchscreen, move the player down.
        {
            if(!player.getPlaying())
            {
                player.setPlaying(true);
            }
            else
            {
                player.setUp(true);
            }
            return true;
        }

        if(event.getAction() == MotionEvent.ACTION_UP)          //if the player is playing and is also touching the screen, move the player up.
        {
            player.setUp(false);
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void update()
    {
        if(player.getPlaying())
        {
            bg.update();
            player.update();

            maxBorderHeight = 30 +player.getScore()/difficultyProgression;

            if(maxBorderHeight > HEIGHT/4)
            {
                maxBorderHeight = HEIGHT / 4;
                minBorderHeight = 5 + player.getScore() / difficultyProgression;
            }

            this.updateUpperBorder();
            this.updateBottomBorder();

            long obstacleElapsed = (System.nanoTime() - obstacleStartTime) / 1000000;
            if(obstacleElapsed >(2000 - player.getScore()/4))
            {

                if(obstacles.size()==0)
                {
                    //have first obstacle in the middle every time
                    obstacles.add(new Obstacle(BitmapFactory.decodeResource(getResources(), drawable.
                            missile),WIDTH + 10, HEIGHT/2, 45, 15, player.getScore(), 13));         //takes in obstacle image, its width and height for collisions and number of frames if any
                }
                else
                {
                    //
                    obstacles.add(new Obstacle(BitmapFactory.decodeResource(getResources(), drawable.missile),
                            WIDTH+10, (int)(rand.nextDouble()*(HEIGHT)),45,15, player.getScore(),13));
                }

                //reset timer
                obstacleStartTime = System.nanoTime();
            }




                long elapsed = (System.nanoTime() - smokeStartTime) / 1000000;
                if (elapsed > 120)
                {

                    smoke.add(new HelicopterSmoke(player.getX(), player.getY() + 10));
                    smokeStartTime = System.nanoTime();


                }

                for (int i = 0; i < smoke.size(); i++)
                {
                    smoke.get(i).update();
                    if (smoke.get(i).getX() < -10)
                    {
                        smoke.remove(i);
                    }
                }

                for (int i = 0; i < obstacles.size(); i++)
                {
                    obstacles.get(i).update();
                    if (collision(obstacles.get(i), player))
                    {
                        obstacles.remove(i);
                        player.setPlaying(false);
                        break;
                    }

                    if (obstacles.get(i).getX() < -100)
                    {
                        obstacles.remove(i);
                        break;
                    }

                }
            }

    }

    public void updateUpperBorder()
    {
        for(int i = 0; i<upperBorder.size(); i++)
        {
            upperBorder.get(i).update();
            if(upperBorder.get(i).getX()<-20)
            {
                upperBorder.remove(i);

                if(upperBorder.get(upperBorder.size()-1).getHeight()>=maxBorderHeight)
                {
                    topDown = false;
                }
                if(upperBorder.get(upperBorder.size()-1).getHeight()<=minBorderHeight)
                {
                    topDown = true;
                }

                if(topDown)
                {
                    upperBorder.add(new UpperBorder(BitmapFactory.decodeResource(getResources(),
                            R.drawable.brick),upperBorder.get(upperBorder.size()-1).getX()+20,
                            0, upperBorder.get(upperBorder.size()-1).getHeight()+1));
                }

                else
                {
                    upperBorder.add(new UpperBorder(BitmapFactory.decodeResource(getResources(),
                            R.drawable.brick),upperBorder.get(upperBorder.size()-1).getX()+20,
                            0, upperBorder.get(upperBorder.size()-1).getHeight()-1));
                }

            }
        }

    }

    public void updateBottomBorder() {


        //update bottom border
        for (int i = 0; i < bottomBorder.size(); i++) {
            bottomBorder.get(i).update();

            //if border is moving off screen, remove it and add a corresponding new one
            if (bottomBorder.get(i).getX() < -20) {
                bottomBorder.remove(i);


                //determine if border will be moving up or down
                if (bottomBorder.get(bottomBorder.size() - 1).getY() <= HEIGHT - maxBorderHeight) {
                    bottomDown = true;
                }
                if (bottomBorder.get(bottomBorder.size() - 1).getY() >= HEIGHT - minBorderHeight) {
                    bottomDown = false;
                }

                if (bottomDown) {

                } else {

                }
            }
        }
    }

    @Override
    public void draw(Canvas canvas)
    {
        final float scaleFactorX = getWidth()/(WIDTH * 1.f);
        final float scaleFactorY = getHeight()/(HEIGHT * 1.f);

        if(canvas!=null) {
            final int savedState = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);
            bg.draw(canvas);
            player.draw(canvas);
            for(HelicopterSmoke sp:smoke)
            {
                sp.draw(canvas);
            }
            for(Obstacle o: obstacles)
            {
                o.draw(canvas);
            }
            canvas.restoreToCount(savedState);
        }
    }

    public boolean collision(GameObject a, GameObject b)
    {
    if(Rect.intersects(a.getRectangle(),b.getRectangle()))
    {
        return true;
    }

        return false;
    }

}
