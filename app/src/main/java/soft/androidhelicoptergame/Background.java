package soft.androidhelicoptergame;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Sam on 30-Apr-16.
 */
public class Background {

    private Bitmap image;   //Background image
    private int x, y, dx;   //Location of background image x and y coordinates



    public Background(Bitmap res) {
        image = res;
        dx = Helicopter_GamePanel.MOVESPEED;
    }

    public void update()
    {
        x+=dx;
        if (x<-Helicopter_GamePanel.WIDTH) {                    //When the backround image falls off the screen, reset its position to the left of the screen again
            x=0;                                                //The left of the screen is x=0
        }
    }

    public void draw(Canvas canvas){
    canvas.drawBitmap(image, x, y, null);
        if(x<0);
        {
            canvas.drawBitmap(image, x+Helicopter_GamePanel.WIDTH, y, null);  //Updates the game every frames instead of the norm
        }
    }


}
