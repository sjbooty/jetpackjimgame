package soft.androidhelicoptergame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Sam on 02/05/2016.
 */
public class HelicopterSmoke extends GameObject {
    public int radius = 5;

    public HelicopterSmoke(int x, int y)
    {
        radius = 5;
        super.x = x;
        super.y = y;
    }

    public void update()
    {
        x-=10;
    }

    public void draw(Canvas canvas)
    {
        Paint paint = new Paint();
        paint.setColor(Color.GRAY);
        paint.setStyle(Paint.Style.FILL);

        canvas.drawCircle(x - radius, y - radius, radius, paint);
        canvas.drawCircle(x - radius + 2, y - radius - 2, radius, paint);
        canvas.drawCircle(x-radius+4, y-radius+1, radius, paint);
    }
}
