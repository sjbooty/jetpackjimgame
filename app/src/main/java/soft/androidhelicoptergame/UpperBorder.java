package soft.androidhelicoptergame;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Sam on 04/05/2016.
 */
public class UpperBorder extends GameObject {
    private Bitmap border;

    public UpperBorder(Bitmap res, int x, int y, int h)
    {
        height = h;
        width = 20;
        this.x = x;
        this.y = y;
        dx = Helicopter_GamePanel.MOVESPEED;
        border = Bitmap.createBitmap(res, 0, 0, width, height);
    }

    public void update()
    {
        x+=dx;
    }
    public void draw(Canvas canvas)
    {
        try
        {
            canvas.drawBitmap(border, x, y, null);
        } catch (Exception ex)
        {

        }
    }

}
