package soft.androidhelicoptergame;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

/**
 * Created by Sam on 04/05/2016.
 */
public class BottomBorder extends GameObject {
    private Bitmap BottomBorder;


    public void BottomBorder(Bitmap res, int x, int y)
    {
        height = 200;
        width = 20;

        this.x = x;
        this.y = y;
        dx = Helicopter_GamePanel.MOVESPEED;

        BottomBorder = Bitmap.createBitmap(res, 0, 0, width, height);
    }

    public void update()
    {
        x+=dx;
    }

    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(BottomBorder, x, y, null);
    }
}
