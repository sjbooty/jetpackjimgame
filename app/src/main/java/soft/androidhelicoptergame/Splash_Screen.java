package soft.androidhelicoptergame;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sam on 03/05/2016.
 */
public class Splash_Screen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        final Intent intent = new Intent(this, Main_Menu.class);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.splash_screen);


        TimerTask task = new TimerTask()
        {

            @Override
        public void run()
            {
            startActivity(intent);

            }
        };
        mp.start();
        Timer splash = new Timer();
        splash.schedule(task, 6000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //getMenuInflater().inflate(R.menu.);
        return true;
    }

}
