package soft.androidhelicoptergame;

import android.graphics.Rect;

/**
 * Created by Sam on 01-May-16.
 */
public abstract class GameObject {
    protected int x;   //x position of gameobject
    protected int y;   //y position of gameobject
    protected int dy;
    protected int dx;
    protected int width;    //width of gameobject
    protected int height;   //height of gameobject


    public void setX(int x) {
    }

    public void setY(int y) {
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }
    public Rect getRectangle()
    {
        return new Rect(x, y, x + width, y + height);   //returns the size of the box around an image (game object) to give the four corners
    }

}
